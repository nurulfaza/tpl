package com.example.helloworld;

public class TrueFalse {

    public TrueFalse() {
    }

    public static String convert(String input){
        if (input.equals("True")) {
            return "False";
        }
        else {
            return "True";
        }
    }
}
