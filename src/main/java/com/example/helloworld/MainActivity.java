package com.example.helloworld;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import static android.provider.AlarmClock.EXTRA_MESSAGE;

public class MainActivity extends AppCompatActivity {
    private Button button;
    private TextView textResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ConstraintLayout constraintLayout = findViewById(R.id.mainLayout);
        AnimationDrawable animationDrawable = (AnimationDrawable) constraintLayout.getBackground();
        animationDrawable.setEnterFadeDuration(1000);
        animationDrawable.setExitFadeDuration(1000);
        animationDrawable.start();

        textResult = (TextView) findViewById(R.id.textResult);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button:
                button = (Button) findViewById(R.id.button);
                textResult.setText(TrueFalse.convert(textResult.getText().toString()));
                break;
        }
    }

    public void exitOnClick(View view) {
        finish();
    }

    @Override
    public void onBackPressed() {
        Toast.makeText(this, "Please click exit button to exit", Toast.LENGTH_SHORT)
                .show();
    }

    public void nextActivity(View view) {
        Intent intent = new Intent(this, SecondActivity.class);
        startActivity(intent);
    }

}
