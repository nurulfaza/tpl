package com.example.helloworld;

import org.junit.Test;

import static org.junit.Assert.*;

public class TrueFalseUnitTest {

    @Test
    public void convert_isCorrect() {
        assertEquals("False", TrueFalse.convert("True"));
        assertEquals("True", TrueFalse.convert("False"));
    }

    @Test
    public void convert_isWrong() {
        assertNotEquals("False", TrueFalse.convert("False"));
        assertNotEquals("True", TrueFalse.convert("True"));
    }
}